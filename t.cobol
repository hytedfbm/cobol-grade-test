000100 IDENTIFICATION DIVISION.
       PROGRAM-ID. GRADES.

000400 DATA DIVISION.
000500 WORKING-STORAGE SECTION.
           01 CNT1    PIC 9(3) VALUE 0.
           01 NAME    PIC X(20).
           01 GRAD    PIC 9(3).
             88 PAS VALUES ARE 51 THRU 100.
             88 FAL VALUES ARE 0  THRU 50.


000700 PROCEDURE DIVISION.
           PERFORM A-PARA 5 TIMES.
           STOP RUN.

       A-PARA.
           PERFORM GET-NAME.

           DISPLAY "GRADE: "
           ACCEPT GRAD.
           
           IF PAS
             DISPLAY NAME " has PASSED".

           IF FAL
             DISPLAY NAME " has FAILED".


       GET-NAME.
           DISPLAY "NAME: "
           ACCEPT NAME.

           INITIALIZE CNT1.
           INSPECT NAME TALLYING CNT1 FOR TRAILING ' '.
           IF CNT1 = 20 STOP RUN.
           DISPLAY CNT1.
           